<?php 
/**
 * The template for the landing page
 *
 * @@Add by Dennis
*/
 ?>

 <?php get_header(); ?>
<!-- @@Add by Dennis -->
<div class="container-fluid">
	<div class="row">
		<div class="col-md-9">
			 <section class="front-page">
			 	<!-- <h3>This is the Landing page.</h3> -->
			 	<div class="container-fluid front-page-container">
			 		<div class="row">
				 		<div class="callout callout-green">
				 			<h3>Latest News & Promotions</h3>
				 			<div class="container-fluid">
				 				<div class="row">
				 					<div class="col-md-12">
							 			<div class="container-fluid">
							 				<div class="row">
								 			<?php query_posts('category_name=news-promotions&posts_per_page=3'); ?>
								 			<?php while ( have_posts() ) : the_post(); ?>
							 					<div class="col-md-4">
									 				<div class="entry-content">
														<a href="<?php echo esc_url( get_permalink() ); ?>">
															<?php if (has_post_thumbnail()) {
																the_post_thumbnail();
															} ?>
														</a>
									 					<h4>
															<a class="callout-link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
														</h4>
														<a class="btn btn-seemore callout-link" href="<?php the_permalink(); ?>">
															<span class="seemore">SEE MORE</span>
														</a>
														<?php //the_excerpt(); ?>
									 				</div>
							 					</div>
											<?php endwhile; ?>
							 				</div>
							 			</div>
				 					</div>
				 				</div>
				 			</div>
						</div>
					</div>

			 		<div class="row">
				 		<div class="callout callout-green">
				 			<h3>Leagues</h3>
				 			<div class="container-fluid">
				 				<div class="row">
				 					<div class="col-md-12">
							 			<div class="container-fluid">
							 				<div class="row">
								 			<?php query_posts('category_name=leagues&posts_per_page=3'); ?>
								 			<?php while ( have_posts() ) : the_post(); ?>
							 					<div class="col-md-4">
									 				<div class="entry-content">
														<a href="<?php echo esc_url( get_permalink() ); ?>">
															<?php if (has_post_thumbnail()) {
																the_post_thumbnail();
															} ?>
														</a>
									 					<h4>
															<a class="callout-link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
														</h4>
														<a class="btn btn-seemore callout-link" href="<?php the_permalink(); ?>">
															<span class="seemore">SEE MORE</span>
														</a>
														<?php //the_excerpt(); ?>
									 				</div>
							 					</div>
											<?php endwhile; ?>
							 				</div>
							 			</div>
				 					</div>
				 				</div>
				 			</div>
						</div>
					</div>

			 		<div class="row">
				 		<div class="callout callout-green">
				 			<h3>Bonspiels</h3>
				 			<div class="container-fluid">
				 				<div class="row">
				 					<div class="col-md-12">
							 			<div class="container-fluid">
							 				<div class="row">
								 			<?php query_posts('category_name=bonspiels&posts_per_page=3'); ?>
								 			<?php while ( have_posts() ) : the_post(); ?>
							 					<div class="col-md-4">
									 				<div class="entry-content">
														<a href="<?php echo esc_url( get_permalink() ); ?>">
															<?php if (has_post_thumbnail()) {
																the_post_thumbnail();
															} ?>
														</a>
									 					<h4>
															<a class="callout-link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
														</h4>
														<a class="btn btn-seemore callout-link" href="<?php the_permalink(); ?>">
															<span class="seemore">SEE MORE</span>
														</a>
														<?php //the_excerpt(); ?>
									 				</div>
							 					</div>
											<?php endwhile; ?>
							 				</div>
							 			</div>
				 					</div>
				 				</div>
				 			</div>
						</div>
					</div>			
			 	</div>
			 </section>
		</div>
	 	<!-- @@Add by Dennis -->
		<div class="col-md-3">
	 		<?php get_sidebar(); ?>
		</div>
	</div>
</div>
 <?php get_footer(); ?>